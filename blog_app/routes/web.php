<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main route
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Home     route    controller@method
Route::get('/home', 'HomeController@index')->name('home');

// Post
Route::get('/posts/create', 'PostController@create'); //create
Route::post('/posts', 'PostController@store'); //save
Route::get('/posts', 'PostController@index'); //show all posts
Route::get('/posts/my-posts', 'PostController@myPosts'); //show all posts of the user, custom method
Route::get('/posts/all-archive', 'PostController@allArchive'); //show all posts archived, custom method
Route::get('/posts/{post_id}', 'PostController@show'); //show individual post
Route::get('/posts/{post_id}', 'PostController@show');
Route::put('/posts/{post_id}', 'PostController@update'); //update logic
Route::get('/posts/{post_id}/edit', 'PostController@edit'); //edit form

//Delete
Route::delete('/posts/{post_id}', 'PostController@destroy');

Route::put('/posts/{post_id}/archive', 'PostController@archive'); //custom method, archive

