<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
        $this->middleware('is.post.owner')->except(['index','show','create','myPosts','allArchive']);
    }
    
    // Create a new post
    // Endpoint: GET /posts/create
    public function create()
    {
        return view('posts.create');
    }

    // Endpoint: POST /posts
    public function store(Request $req)
    {
        // Check the responsiveness of the link
            // dd ($req);

        // Create a new post object
        $new_post = new Post([
            'title' => $req->input('title'),
            'content' => $req->input('content'),
            'user_id' => Auth::user()->id
        ]);

        // Save it to the database
        $new_post->save();

        // Redirect the user somewhere
        return redirect('/posts/create');
    }

    // Endpoint: GET /posts
    public function index()
    {
        // dd(Post::all());
        $posts_list = Post::all();
                                        //identifier   //data
        return view('posts.index')->with('posts', $posts_list);
    }

    // Endpoint: GET /posts/{post_id}
    public function show($post_id)
    {
            // dd($post_id);
        // Retrieve a specific post
            $post = Post::find($post_id);
            // dd($post);
            return view('posts.show')->with('post', $post);

    }

    // Endpoint: GET /posts/my-posts
    public function myPosts()
    {
            // dd (Auth::user()->posts);
        $my_posts = Auth::user()->posts;
        return view('posts.index')->with('posts', $my_posts);
    }

    // Endpoint: GET /posts/{post_id}/edit
    public function edit($post_id)
    {
        // Find the post to be updated
            $existing_post = Post::find($post_id);
        // Redirect the user to page where the post will be updated
            return view('posts.edit')->with('post', $existing_post);
    }

    // Endpoint: PUT /posts/{post_id}
    public function update($post_id, Request $req)
    {
        // Find an existing post to be updated
            $existing_post = Post::find($post_id);
        // Set the new values of an existing post
            $existing_post->title = $req->input('title');
            $existing_post->content = $req->input('content');
            $existing_post->save();
        // Redirect the user to the page of individual post
            return redirect("/posts/$post_id");
    }

    // Endpoint: DELETE /posts/{post_id}
    public function destroy($post_id)
    {
        // Find the existing post to be deleted
            $existing_post = Post::find($post_id);
            //dd($existing_post);
        // Delete the post
            $existing_post->delete();
        // Redirect the user somewhere
            return redirect('/posts');
    }

    // Endpoint: /posts/{post_id}/archive
    public function archive($post_id)
    {
        // Find the existing post to be archives
            $existing_post = Post::find($post_id);
            //dd($existing_post);
        // Archive the post
            $existing_post->is_active = false;
            $existing_post->save();
        // Redirect the user somewhere
            return redirect("/posts/$post_id");
    }

    // Endpoint: GET /posts/all-archive
    public function allArchive()
    {
        // $archived = Post::where('is_active',0)->get();
        // return view('posts.archived')->with('posts', $archived);

        $archived_posts = Auth::user()->posts;
        return view('posts.archived')->with('archived_posts', $archived_posts);

    }

}